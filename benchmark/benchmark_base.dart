library benchmark;

import "dart:async";
import "dart:math";
import "dart:html";

class BenchmarkBase {
	void setUp() {}
	void tearDown() {}
	void test() {}

	double run({int runs: 1000,bool setUpEachRun: false}) {
		int cumulatedTime = 0;
		if (setUpEachRun == false)
			setUp();
		Stopwatch totalWatch = new Stopwatch()..start();
		for (int i=0;i<runs;++i) {
			if (setUpEachRun)
				setUp();
			Stopwatch watch = new Stopwatch()..start();
			test();
			cumulatedTime += watch.elapsedMicroseconds;
			if (setUpEachRun)
				tearDown();
		}
		totalWatch.stop();
		if (setUpEachRun == false)
			tearDown();
		return setUpEachRun ? cumulatedTime/runs : totalWatch.elapsedMicroseconds/runs;
	}
}