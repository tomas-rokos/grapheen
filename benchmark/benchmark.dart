import "dart:async";

import "dart:html";
import "dart:math";
import "create_vertex.dart";
import "load_dogs.dart";
import "query_dog_by_id.dart";
import "query_dogs_by_home_name.dart";
import "query_dogs_by_multiple_params.dart";
import "query_dogs_from_breeder.dart";
import "query_dogs_from_breeder_by_edge_name.dart";

Future main() async {
	String data = await HttpRequest.getString("dogs.csv");

	new Timer.periodic(new Duration(milliseconds: 100),(_) {
		runTests(data);
	});
}

Map results = new Map();

String _formatNumber(int t) {
	if (t == 0)
		return "      0";
	int l = (log(t)/LN10).toInt()+1;
	if (t<1000)
		--l;
	String s = "";
	for (int i=l;i<6;++i) {
		s += " ";
	}
	if (t>999) {
		s += (t ~/1000).toString();
		s += ",";
		t = t%1000;
		if (t<99)
			s += "0";
		if (t<9)
			s += "0";
	}
	s += t.toString();
	return s;
}

void registerResult(String text, double micros) {
	if (results.containsKey(text) == false) {
		PreElement elem = new PreElement();
		document.body.append(elem);
		Map item = new Map();
		item["count"]  = 0;
		item["micros"] = 0.0;
		item["elem"] = elem;
		results[text] =  item;
	}
	Map item = results[text];
	item["count"] += 1;
	item["micros"] += micros;
	double smicros = item["micros"] / item["count"];
	int ops = (1000000 / smicros).toInt();
	item["elem"].innerHtml = _formatNumber(ops)+" : "+text+" ( "+smicros.toString()+" µs ) based on "+item["count"].toString();
}

void runTests(String data) {
	registerResult("CreateVertex", new CreateVertex().run(setUpEachRun:true));
	registerResult("LoadDogs",new LoadDogs(data).run(runs:1));
	registerResult("QueryDogById",new QueryDogById(data).run());
	registerResult("QueryDogsByHomeName",new QueryDogsByHomeName(data).run());
	registerResult("QueryDogsByMultipleParams",new QueryDogsByMultipleParams(data).run());
	registerResult("QueryDogsFromBreeder",new QueryDogsFromBreeder(data).run());
	registerResult("QueryDogsFromBreederByEdgeName",new QueryDogsFromBreederByEdgeName(data).run());
}