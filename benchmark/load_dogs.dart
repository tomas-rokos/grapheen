import "benchmark_base.dart";
import "dart:async";
import "dart:html";
import "package:grapheen/grapheen.dart" as gr;

class LoadDogs extends BenchmarkBase {
	gr.Grapheen g;
	String data;
	LoadDogs(this.data);
	void setUp()  {
	}

	void tearDown() {
	}

	void test() {
		g = new gr.Grapheen();
		g.loadGraph(data);
		g = null;
	}

}
