import "benchmark_base.dart";
import "dart:async";
import "package:grapheen/grapheen.dart" as gr;
import "dart:html";

class QueryDogsFromBreederByEdgeName extends BenchmarkBase {
	gr.Grapheen g;
	String data;
	QueryDogsFromBreederByEdgeName(this.data);
	void setUp() {
		g = new gr.Grapheen();
		g.loadGraph(data);
	}

	void tearDown() {
		g = null;
	}

	void test() {
		gr.Pipe v = g.V(1000001).inE("breeder").outV();
		v.ids();
	}

}
