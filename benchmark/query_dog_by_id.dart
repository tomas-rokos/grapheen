import "benchmark_base.dart";
import "dart:async";
import "package:grapheen/grapheen.dart" as gr;
import "dart:html";

class QueryDogById extends BenchmarkBase {
	gr.Grapheen g;
	String data;
	QueryDogById(this.data);
	void setUp() {
		g = new gr.Grapheen();
		g.loadGraph(data);
	}

	void tearDown() {
		g = null;
	}

	void test() {
		gr.Pipe v = g.V(8141);
		v.map();
	}

}
