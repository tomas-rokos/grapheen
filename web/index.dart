import "package:grapheen/grapheen.dart";
import "package:grapheen/visualiser.dart";
import "../test/bands.dart";
import "dart:html";

void main() {
	Grapheen g = new Grapheen();
	fillInGraph(g);

	DivElement div = querySelector("#graph");
	div.style.position = "absolute";
	div.style.top = "0px";
	div.style.left = "0px";
	div.style.width = document.documentElement.clientWidth.toString()+"px";
	div.style.height = document.documentElement.clientHeight.toString()+"px";

	Visualiser v = new Visualiser(g,div);

	v.render(5, new Point(100,100));
	v.render(3,new Point(400,100));

}