GRAPH Embeddable ENgine with Gremlin API, with no additional dependencies. 

See `test` folder for a lot of examples of usage.

```dart
import 'package:grapheen/grapheen.dart';
...
Grapheen g = new Grapheen();
Pipe vIM = g.addVertex({"name":"Iron Maiden"}); // id=1
Pipe vHM = g.addVertex({"name":"Heavy Metal"}); // id=2
Pipe e1 = g.addEdge(vIM,vHM,"PLAYS"); // id=3
List<int> ids;
ids = g.V(1).outE().inV().ids(); // [2]
ids = g.V("name","Iron Maiden").outE("PLAYS").inV().ids(); // [2]
g.V(1).setProperty("name","Iron 'The best' Maiden");
g.E(3).remove();
```

Functionality
=============
I am implementing the Gremlin API as needed for my projects, so far I have:

Adding to graph:  **addVertex** , **addEdge**

Changing the graph:  **setProperty**, **remove**

Traversing the graph:  **V**, **E**, **inE**, **outE**, **bothE**, **inV**, **outV**, **bothV**

Filtering pipeline:  **has**, **hasNot**, **IN**, **CONTAINS**

Transforming pipeline: **dedup**, **order** (just the property based) 

Performance
===========
Since the most expected usage is inside the web browser, compiled 
to javascript. All measurements are in compiled mode in Opera on my MacBook.

- Sample data is snippet of my dogs database, it has 4.300 vertexes and about 12.000 edges.
- number is OPERATIONS PER SECONDS, so 500 means, that such workflow, will run 500 times in one second 

```
-----WORKFLOW------------ MEMORY--------WORKFLOW------------- MEMORY---
createVertex          |    87.896 |                       |           |     
load data set         |         5 |                       |           |
query by ID           |   362.894 |                       |           |
query by property     | 1.103.439 |                       |           |
query by two props    |    30.687 |                       |           |
query by edge         |   141.486 |                       |           |
query by named edge   |    76.670 |                       |           |
-----------------------------------------------------------------------
```

References
==========

- <https://github.com/tinkerpop/gremlin>

- <http://gremlindocs.com>