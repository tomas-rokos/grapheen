// for publish ~/dart/dart-sdk/bin/pub publish --dry-run
library grapheen;

import "dart:async";
import "dart:convert";
import "dart:math";
import "loader_csv.dart";

part "src/indexer.dart";
part "src/pipe.dart";
part "src/pipe_ids.dart";
part "src/pipe_q.dart";
part "src/pipe_in_out_v.dart";
part "src/pipe_in_out_e.dart";
part "src/pipe_dedup.dart";
part "src/pipe_order.dart";
part "src/utils.dart";
part "replication.dart";
part "replication_with_reserve_ids.dart";

abstract class Pipe {
	// initial point to graph
	Pipe V([var idOrKey, var value]);
	Pipe E([var idOrKey, var value]);
	// traversal thru graph
	Pipe inE([String name]);
	Pipe inV();
	Pipe outE([String name]);
	Pipe outV();
	Pipe bothE([String name]);
	Pipe bothV();
	// condition
	//
	// conditions: = , != , IN ,
	Pipe has(String key,var valueOrCondition,[var value]);
	Pipe hasNot(String key,var value);
	// filter
	Pipe dedup();
	// transformation
	Pipe order(String key);

	Map map();
	bool setProperty(String name,var value);
	bool remove();

	List ids(); // id of objects representing this Pipe
	int first_id();

//	Future<List<List>> path(); // list of paths to the current Pipe
}

class Grapheen {
	Replication _repli;
	set replication(Replication r) {_repli = r;}
	Replication get replication => _repli;

	_Indexer _v = new _Indexer();
	_Indexer _e = new _Indexer();

	int _nextObjectId = 1;
	int newObjectId() {
		return _repli != null ? _repli.newObjectId() : _nextObjectId++;

	}
	void _set(bool vertex,int id,String key, Object value) {
		if (vertex == true) {
			_v.set(id,key,value);
		} else {
			_e.set(id,key,value);
		}
		if (_repli != null)
			_repli.addToQueue(vertex,id,key,value);
	}
	Pipe addVertex([Map properties]) {
		int id = newObjectId();
		if (properties != null) {
			properties.forEach((String k, var v) {
				_set(true,id,k,v);
			});
		}
		return new _PipeIds(this,[id]).._vertex = true;
	}
	Pipe addEdge(Pipe from,Pipe to, String name,[Map properties] ) {
		List fromIds = from.ids();
		List toIds = to.ids();
		List eids = new List();
		fromIds.forEach((int fromid) {
			toIds.forEach((int toid) {
				int id = newObjectId();
				_set(false,id,'_from',fromid);
				_set(false,id,'_to',toid);
				_set(false,id,'_name',name);
				if (properties != null) {
					properties.forEach((String k, var v) {
						_set(false,id,k,v);
					});
				}
				eids.add(id);
			});
		});
		return new _PipeIds(this,eids).._vertex = false;
	}
	Pipe V([var idOrKey, var value]) {
		return new _Pipe(this,null).V(idOrKey,value);
	}
	Pipe E([var idOrKey, var value]) {
		return new _Pipe(this,null).E(idOrKey,value);
	}
	void loadGraph(String s) {
		LoaderCSV loader =  new LoaderCSV(s);
		while (true) {
			List<String> row = loader.splitLine();
			if (row == null)
				return;
			int rid = 0;
			try {
				rid = int.parse(row[1]);
			} catch (e) {
				print("ERROR("+row[1]+"):really strange ID:");
				continue;
			}
			if (rid >= _nextObjectId)
				_nextObjectId = rid+1;
			for (int i = 2;i<row.length;i+=2) {
				Object val;
				if (i+1 >=row.length) {
					print ("ERROR("+rid.toString()+"): no property pair");
					continue;
				}
				String vals = row[i+1];
				if (vals.codeUnitAt(0) == 39) {
					String subVal = vals.substring(1,vals.length-1);
					val = subVal.replaceAll("\\'","'");
				} else {
					try {
						val = int.parse(vals);
					} catch (e) {
						print("ERROR("+rid.toString()+"): cannot covert number: "+vals);
					}
				}
				if (row[0] == "V") {
					_v.set(rid,row[i],val);
				} else {
					_e.set(rid,row[i],val);
				}
			}
		}

	}
	void clear() {
		_v = new _Indexer();
		_e = new _Indexer();
		_nextObjectId = 1;
		if (_repli != null)
			_repli.clear();
	}
	String dump() {
		List<int> ids = new List.from(_v._objects.keys);
		ids.addAll(_e._objects.keys);
		ids.sort();
		String s = "";
		ids.forEach((int i) {
			Map obj = null;
			if (_v._objects.containsKey(i)) {
				obj = _v._objects[i];
				s+= "V,"+i.toString();
			} else if (_e._objects.containsKey(i)) {
				obj = _e._objects[i];
				s+= "E,"+i.toString();
			} else
				return;
			obj.forEach((String key,Object val) {
				s +=","+key+",";
				if (val is String) {
					val = val.replaceAll("'","\\'");
					s += "'"+val+"'";
				}
				else
					s += val.toString();
			});
			s += "\n";

		});

		return s;
	}
	Map _getObject(int id) {
		if (_v._objects.containsKey(id)) {
			return _v._objects[id];
		} else if (_e._objects.containsKey(id)) {
			return _e._objects[id];
		}
		return {};
	}
	String stats() {
		String s = "";
		s += "V:"+_v._objects.length.toString();
		s += "E:"+_e._objects.length.toString();
		return s;
	}
}
