library grapheen_visualiser;

import "grapheen.dart";
import "dart:html";
import "dart:svg" as svg;
import "dart:math";

part "src/vis/vertex.dart";
part "src/vis/edge.dart";

class Visualiser {
	Grapheen _g;
	svg.SvgSvgElement _svg;

	bool _inDrag = false;
	List<Vertex> _selection = new List();
	Point _lastPos;

	Visualiser(this._g,DivElement div) {
		_svg = new svg.SvgSvgElement();
		div.append(_svg);
		_svg.style.width = div.style.width;
		_svg.style.height = div.style.height;

		_svg.onMouseMove.listen((MouseEvent e) {
			if (_inDrag == false)
				return;
			int dx = e.client.x-_lastPos.x;
			int dy = e.client.y-_lastPos.y;
			_lastPos = e.client;
			_selection.forEach((Vertex v) {
				Point curr = v.position;
				v.position = new Point(curr.x+dx,curr.y+dy);
				_edges.forEach((int eId,Edge edge) {edge._render();});
			});
		});
		_svg.onMouseUp.listen((MouseEvent e) {
			_inDrag = false;
		});
//		_svg.onMouseOut.listen((MouseEvent e) {
//			_inDrag = false;
//		});
	}
	void _onStartMove(Vertex v,MouseEvent e) {
		_inDrag = true;
		_lastPos = e.client;
		_selection.clear();
		_selection.add(v);
	}
	void _exploreEdges(Vertex v,bool out) {
		List<int> ids = out ? v.pipe.outE().inV().ids() : v.pipe.inE().outV().ids();
		Point orig = new Point(v.position.x + (out ? 250 : -250),v.position.y-50);
		if (orig.x < 0 || orig.y < 0)
			orig = new Point(0,0);
		ids.forEach((int id) {
			render(id,orig);
			orig = new Point(orig.x,orig.y + 50);
		});
	}
	void _setAttributes(svg.SvgElement g,Map attr) {
		attr.forEach((String key, String val) {
			g.setAttribute(key,val);
		});
	}
	Map<int,Vertex> _vertexes = new Map();
	Map<int,Edge> _edges = new Map();
	Vertex render(int key,[Point pnt]) {
		if (_vertexes.containsKey(key))
			return _vertexes[key];
		Pipe p = _g.V(key);
		Vertex v = new Vertex(this,p,pnt != null ? pnt : new Point(0,0));
		_vertexes[key] = v;

		List<int> outE =  p.outE().ids();
		List<int> inE = p.inE().ids();

		outE.forEach((int outEid) {
			if (_edges.containsKey(outEid) == true)
				return;
			Map props = _g.E(outEid).map();
			int toId = props["_to"];
			if (_vertexes.containsKey(toId) == true) {
				Edge edge = new Edge(this,v,_vertexes[toId],outEid);
				_edges[outEid] = edge;
			}
		});

		inE.forEach((int inEid) {
			if (_edges.containsKey(inEid) == true)
				return;
			Map props = _g.E(inEid).map();
			int fromId = props["_from"];
			if (_vertexes.containsKey(fromId) == true) {
				Edge edge = new Edge(this,_vertexes[fromId],v,inEid);
				_edges[inEid] = edge;
			}
		});

		return v;
	}
}