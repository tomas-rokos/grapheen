library grapheen.element_wrapper;

import "grapheen.dart" as grapheen;
import "dart:math";

class ElementWrapper {
	grapheen.Pipe _pipe;
	grapheen.Pipe get pipe => _pipe;

	int get id => _pipe.first_id();
	Map _cachedmp;
	Map get raw {
		_cache();
		return new Map.from(_cachedmp);
	}
	ElementWrapper(grapheen.Grapheen gg, int id) {
		_pipe = gg.V(id);
	}
	ElementWrapper.edge(grapheen.Grapheen gg, int id) {
		_pipe = gg.E(id);
	}
	ElementWrapper.createVertex(grapheen.Grapheen gg) {
		_pipe = gg.addVertex();
	}
	ElementWrapper.createEdge(grapheen.Grapheen gg,int fromId,int toId, String name) {
		_pipe = gg.addEdge(gg.V(fromId),gg.V(toId),name);
	}
	void _cache() {
		if (_cachedmp == null)
			_cachedmp = _pipe.map();
	}
	void invalidateCache() {
		_cachedmp = null;
	}

	//
	//	direct data access operator
	//
	Object operator [](String key) {
		_cache();
		return _cachedmp[key];
	}
	void operator []=(String key, Object value) {
		invalidateCache();
		if (value is Point)
			_pipe.setProperty(key,value.x.toString()+","+value.y.toString());
		else
			_pipe.setProperty(key,value);
	}

	//
	//	safe typed access methods
	//
	String getString(String key) {
		_cache();
		if (_cachedmp.containsKey(key) == false)
			return "";
		Object val = _cachedmp[key];
		if (val is String)
			return val;
		return val.toString();
	}
	DateTime getDateTime(String key) {
		String s = getString(key);
		return s == "" ? null : DateTime.parse(s);
	}
	Point getPoint(String key) {
		String v = getString(key);
		int pos = v.indexOf(",");
		if (pos == -1)
			return new Point(0,0);
		int x = int.parse(v.substring(0,pos));
		int y = int.parse(v.substring(pos+1));
		return new Point(x,y);
	}
}