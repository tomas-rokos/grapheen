part of grapheen;

class _PipeOrder extends _Pipe {
	String _prop;

	bool get vertexType => _prev.vertexType;

	_PipeOrder(Grapheen g,_Pipe prev,this._prop):super(g,prev);

	List ids() {
		List<int> inp = _prev.ids();
		List<List> vals = new List();
		_Indexer i = vertexType ? _g._v : _g._e;
		inp.forEach((int id) {
			Map obj = i.get(id);
			Object prop = obj.containsKey(_prop) == true ? obj[_prop] : null;
			vals.add([id,prop]);
		});
		vals.sort((List a, List b) {
			Object as = a[1];
			Object bs = b[1];
			return _Utils.compare(as,bs);
		});
		inp.clear();
		vals.forEach((List val) {
			inp.add(val[0]);
		});
		return inp;
	}
}

