part of grapheen;

class _PipeInOutV extends _Pipe {
	bool _in;
	bool _out;

	bool get vertexType => true;


	_PipeInOutV(Grapheen g,_Pipe prev,this._in,this._out):super(g,prev);

	List ids(){
		List<int> res = new List();
		List<int> inp = _prev.ids();

		inp.forEach((int eid) {
			if (_in) {
				int vid = _g._e.get(eid)["_to"];
				res.add(vid);
			}
			if (_out) {
				int vid = _g._e.get(eid)["_from"];
				res.add(vid);
			}
		});
		return res;
	}
}

