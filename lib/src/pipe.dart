part of grapheen;

class _Pipe implements Pipe {
	Grapheen _g;
	_Pipe _prev;
	bool _vertex;
	bool get vertexType => _vertex;

	// exact pipe constructor
	_Pipe(this._g,this._prev);

	Pipe V([var idOrKey, var value]) {
		_vertex = true;
		if (idOrKey is int) {
			return new _PipeIds(_g,[idOrKey]).._vertex = true;
		} else if (idOrKey is String) {
			return new _PipeQ(_g,this,idOrKey,"=",value).._vertex = true;
		}
		return this;
	}
	Pipe E([var idOrKey, var value]) {
		_vertex = false;
		if (idOrKey is int) {
			return new _PipeIds(_g,[idOrKey]).._vertex = false;
		}
		return this;
	}
	Pipe inE([String name]) {
		return new _PipeInOutE(_g,this,true,false,name);
	}
	Pipe inV() {
		return new _PipeInOutV(_g,this,true,false);
	}
	Pipe outE([String name]) {
		return new _PipeInOutE(_g,this,false,true,name);
	}
	Pipe outV() {
		return new _PipeInOutV(_g,this,false,true);
	}
	Pipe bothE([String name]) {
		return new _PipeInOutE(_g,this,true,true,name);
	}
	Pipe bothV() {
		return new _PipeInOutV(_g,this,true,true);
	}
	Pipe has(String key,var valueOrCondition,[var value]) {
		if (value == null) {
			if (valueOrCondition is List)
				return new _PipeQ(_g,this,key,"IN",valueOrCondition);
			else
				return new _PipeQ(_g,this,key,"=",valueOrCondition);
		}
		return new _PipeQ(_g,this,key,valueOrCondition,value);
	}
	Pipe hasNot(String key,var value) {
		return new _PipeQ(_g,this,key,"!=",value);
	}
	Pipe dedup() {
		return new _PipeDedup(_g,this);
	}
	Pipe order(String key) {
		return new _PipeOrder(_g,this,key);
	}
	Map map() {
		List<int> resids = ids();
		if (resids.length>0) {
			Map objMap = _g._getObject(resids[0]);
			return objMap != null ? new Map.from(objMap) : null;
		}
		return null;
	}
	bool setProperty(String name,var value) {
		List<int> resids = ids();
		List recs = new List();
		resids.forEach((int id) {
			_g._set(vertexType,id,name,value);
		});
		return true;
	}
	bool remove() {
		List<int> resids = ids();
		List recs = new List();
		resids.forEach((int id) {
			Map obj = new Map.from(_g._getObject(id));
			obj.keys.forEach((String prop) {
				_g._set(vertexType,id,prop,null);
			});
		});
		if (vertexType == true) {
			List<int> edgeids = [];
			resids.forEach((int id) {
				edgeids.addAll(_g._e.lookup("_from",id,"="));
				edgeids.addAll(_g._e.lookup("_to",id,"="));
			});
			Pipe edgesForDelete = new _PipeIds(_g,edgeids);
			edgesForDelete.remove();
		}
		return true;
	}
	List ids() {
		return null;
	}
	int first_id() {
		List<int> idslist = ids();
		return idslist.length == 0 ? 0 : idslist.first;
	}
}

