part of grapheen;

class _Indexer {

	Map<int,Map> _objects = new Map<int,Map>();
	Map get(int id) => _objects[id];
	Map<String,Map<Object,List<int>>> _propIndex = new Map();

	void set(int id,String key,Object value) {
		if (_objects.containsKey(id) == false) {
			if (value == null)
				return;
			else
				_objects[id] = new Map();
		}
		Map obj = _objects[id];
		if (obj.containsKey(key) == true) {
			Object oldVal = obj[key];
			_propIndex[key][oldVal].remove(id);
		}
		if (value == null) {
			obj.remove(key);
			if (obj.length == 0)
				_objects.remove(id);
		} else {
			obj[key] = value;
			Map valueMap = _propIndex[key];
			if (valueMap == null) {
				valueMap = new Map();
				_propIndex[key] = valueMap;
			}
			List valueRes = valueMap[value];
			if (valueRes == null) {
				valueRes = new List();
				valueMap[value] = valueRes;
			}
			valueRes.add(id);
		}
	}
	List<int> lookup(String prop, Object val,String operator) {
		List res = [];
		if (_propIndex.containsKey(prop) == false)
			return res;
		if (operator == "=") {
			res = _propIndex[prop][val];
		} else if (operator == "!=") {
			_propIndex[prop].forEach((Object singleVal,List ids) {
				if (singleVal != val)
					res.addAll(ids);
			});
		} else if (operator == "CONTAINS") {
			_propIndex[prop].forEach((String singleVal,List ids) {
				if (singleVal.contains(val) == true)
					res.addAll(ids);
			});
		} else if (["<",">","<=",">="].contains(operator)) {
			bool lessThen = ["<","<="].contains(operator);
			bool equal = ["<=",">="].contains(operator);
			bool biggerThen = [">",">="].contains(operator);
			_propIndex[prop].forEach((Object singleVal,List ids) {
				int cmp = _Utils.compare(singleVal,val);
				if (cmp == -1 && lessThen || cmp == 0 && equal || cmp == 1 && biggerThen)
					res.addAll(ids);
			});
		}
		return res != null ? res : [];
	}
}