part of grapheen;

class _PipeInOutE extends _Pipe {
	bool _in;
	bool _out;
	String _name;

	bool get vertexType => false;


	_PipeInOutE(Grapheen g,_Pipe prev,this._in,this._out,this._name):super(g,prev);

	List ids() {
		List<int> res = new List();
		List<int> inp = _prev.ids();

		inp.forEach((int vid) {
			if (_in) {
				List<int> sres = _g._e.lookup("_to",vid,"=");
				if (_name != null) {
					sres.forEach((int sresid)  {
						String ename = _g._e.get(sresid)["_name"];
						if (ename == _name)
							res.add(sresid);
					});
				} else {
					res.addAll(sres);
				}
			}
			if (_out) {
				List<int> sres = _g._e.lookup("_from",vid,"=");
				if (_name != null) {
					sres.forEach((int sresid) {
						String ename = _g._e.get(sresid)["_name"];
						if (ename == _name)
							res.add(sresid);
					});
				} else {
					res.addAll(sres);
				}
			}
		});
		return res;
	}
}

