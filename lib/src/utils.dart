part of grapheen;

class _Utils {

	static List<int> do_and(List<int> a, List<int> b) {
		if (a == null)
			return b;
		if (b == null)
			return a;
		List<int> res = new List();
		a.forEach((int k) {
			if (b.contains(k))
				res.add(k);
		});
		return res;
	}

	static List<int> get_ids(List<List> events) {
		List<int> ids = new List<int>();
		events.forEach((List event) {
			if (ids.contains(event[1]))
				return;
			ids.add(event[1]);
		});
		return ids;
	}

	static int compare(Object as, Object bs) {
		if (as == null)
			return bs == null ? 0 : 1;
		if (bs == null)
			return -1;
		if (as is String && bs is String)
			return as.compareTo(bs);
		if (as is num && bs is num)
			return as.compareTo(bs);
		if (as is num && bs is String)
			return -1;
		if (as is String && bs is num)
			return 1;
		return 0;
	}
}