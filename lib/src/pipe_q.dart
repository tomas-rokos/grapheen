part of grapheen;

class _PipeQ extends _Pipe {
	String _prop;
	String _cond;
	var _val;

	bool get vertexType => _prev.vertexType;

	_PipeQ(Grapheen g,_Pipe prev,this._prop,this._cond,this._val):super(g,prev);

	List ids() {
		_Indexer i = vertexType ? _g._v : _g._e;
		List<int> res = [];
		List vals = _val is List ? _val : [_val];
		String cond = _cond;
		if (cond == "IN")
			cond = "=";
		vals.forEach((var val) {
			res.addAll(i.lookup(_prop,val,cond));
		});
		List<int> inp = _prev.ids();
		return _Utils.do_and(res,inp);
	}
}

