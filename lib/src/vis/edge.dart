part of grapheen_visualiser;

class Edge {
	svg.SvgElement _svg_group;

	Visualiser _vis;
	Vertex _from,_to;
	int _id;

	Edge (this._vis,this._from,this._to,this._id) {
		_render();
	}

	void _render() {
		if (_svg_group != null)
			_svg_group.innerHtml = "";
		else
			_svg_group = new svg.GElement();


		Point fromp = _from.position+new Point(_from.width/2,_from.height/2);
		Point top = _to.position+new Point(_to.width/2,_to.height/2);
		Point centerp = new Point(((top.x+fromp.x)/2).toInt(),((top.y+fromp.y)/2).toInt());
		Point startp = _from._closestConnectionPointTo(centerp);
		Point endp = _to._closestConnectionPointTo(centerp);

		Point vec = endp-startp;
		int ang = atan2(vec.y,vec.x)*180~/PI;

		svg.LineElement ln = new svg.LineElement();
		_vis._setAttributes(ln,{"x1":startp.x.toString(),"y1":startp.y.toString(),"x2":endp.x.toString(),"y2":endp.y.toString(),"stroke":"black"});
		_svg_group.append(ln);

		svg.TextElement name = new svg.TextElement();
		_vis._setAttributes(name,{"x":centerp.x.toString(),"y":centerp.y.toString(),"text-anchor":"middle","alignment-baseline":"middle","transform":"rotate("+ang.toString()+","+centerp.x.toString()+","+centerp.y.toString()+")"});
		name.innerHtml = "> > "+_vis._g.E(_id).map()["_name"]+" > >";
		_svg_group.append(name);


		_vis._svg.append(_svg_group);
	}


}