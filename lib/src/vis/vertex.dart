part of grapheen_visualiser;

class Vertex {
    svg.SvgElement _svg_group;

    Visualiser _vis;
    Pipe _pipe;
    Pipe get pipe => _pipe;

    Point _pos = new Point(0,0);
    Point get position => _pos;
    void set position(Point val) {
	    _pos = val;
	    _svg_group.setAttribute("transform","translate("+val.x.toString()+","+val.y.toString()+")");
    }
    int _width;
    int get width => _width;
    int _height;
    int get height => _height;

    Vertex (this._vis,this._pipe,Point loc) {
	    _render();
	    this.position = loc;
    }

    void _render() {
	    _svg_group = new svg.GElement();
	    _width = 200;

	    svg.TextElement tId = new svg.TextElement();
	    _vis._setAttributes(tId,{"x":"45","y":"10","text-anchor":"middle","alignment-baseline":"middle"});
	    tId.innerHtml = "#"+_pipe.ids().first.toString();
	    _svg_group.append(tId);

	    Map mp = _pipe.map();
	    svg.TextElement tLabel = new svg.TextElement();
	    _vis._setAttributes(tLabel,{"x":"75","y":"10","text-anchor":"left","alignment-baseline":"middle"});
	    _svg_group.append(tLabel);

	    svg.TextElement tProps = new svg.TextElement();
	    _vis._setAttributes(tProps,{"y":"20"});
	    _svg_group.append(tProps);

	    svg.TextElement tInEdges = new svg.TextElement();
	    _vis._setAttributes(tInEdges,{"x":"10","y":"10","text-anchor":"middle","alignment-baseline":"middle","font-size":"70%"});
	    tInEdges.innerHtml = _pipe.inE().ids().length.toString()+"->";
	    _svg_group.append(tInEdges);

	    svg.TextElement tOutEdges = new svg.TextElement();
	    _vis._setAttributes(tOutEdges,{"x":"190","y":"10","text-anchor":"middle","alignment-baseline":"middle","font-size":"70%"});
	    tOutEdges.innerHtml = "->"+_pipe.outE().ids().length.toString();
	    _svg_group.append(tOutEdges);

	    int props = 0;
	    mp.forEach((String key, Object val) {
		    if (key == "label") {
			    tLabel.innerHtml = val;
			    return;
		    }
		    svg.TSpanElement tProp = new svg.TSpanElement();
		    _vis._setAttributes(tProp,{"x":"5","dy":"15"});
		    if (val is String) {
			    tProp.innerHtml = key + " : " + val;
		    } else {
			    tProp.innerHtml = key + " : " + val.toString();
		    }
		    tProps.append(tProp);
		    ++props;
	    });

	    _height = 25+props*15;

	    svg.RectElement rect = new svg.RectElement();
	    _vis._setAttributes(rect,{"x":"0","y":"0","width":"200","height":_height.toString(),"fill":"none","stroke":"black"});
	    _svg_group.append(rect);
        svg.RectElement rect1 = new svg.RectElement();
	    _vis._setAttributes(rect1,{"x":"0","y":"0","width":"20","height":"20","fill":"none","stroke":"grey"});
	    _svg_group.append(rect1);
        svg.RectElement rect2 = new svg.RectElement();
	    _vis._setAttributes(rect2,{"x":"20","y":"0","width":"50","height":"20","fill":"none","stroke":"grey"});
	    _svg_group.append(rect2);
        svg.RectElement rect3 = new svg.RectElement();
	    _vis._setAttributes(rect3,{"x":"70","y":"0","width":"110","height":"20","fill":"none","stroke":"grey"});
	    _svg_group.append(rect3);
        svg.RectElement rect4 = new svg.RectElement();
	    _vis._setAttributes(rect4,{"x":"180","y":"0","width":"20","height":"20","fill":"none","stroke":"grey"});
	    _svg_group.append(rect4);

	    svg.RectElement rectMove = new svg.RectElement();
	    _vis._setAttributes(rectMove,{"x":"20","y":"0","width":"160","height":"20","fill":"white","fill-opacity":"0"});
	    _svg_group.append(rectMove);
	    rectMove.style.cursor = "move";
	    rectMove.onMouseDown.listen((MouseEvent e) {
		    _vis._onStartMove(this,e);
	    });

	    svg.RectElement rectInEdges = new svg.RectElement();
	    _vis._setAttributes(rectInEdges,{"x":"0","y":"0","width":"20","height":"20","fill":"white","fill-opacity":"0"});
	    _svg_group.append(rectInEdges);
	    rectInEdges.style.cursor = "pointer";
	    rectInEdges.onMouseDown.listen((MouseEvent e) {
		    _vis._exploreEdges(this,false);
	    });

	    svg.RectElement rectOutEdges = new svg.RectElement();
	    _vis._setAttributes(rectOutEdges,{"x":"180","y":"0","width":"20","height":"20","fill":"white","fill-opacity":"0"});
	    _svg_group.append(rectOutEdges);
	    rectOutEdges.style.cursor = "pointer";
	    rectOutEdges.onMouseDown.listen((MouseEvent e) {
		    _vis._exploreEdges(this,true);
	    });


	    _vis._svg.append(_svg_group);
    }

	Point _closestConnectionPointTo(Point target) {
		double dist = 1e20;
		Point p;
		for (int i=0;i<4;++i) {
			Point con;
			switch (i) {
				case 0: con = _pos + new Point(_width/2,0);break;
				case 1: con = _pos + new Point(_width,_height/2);break;
				case 2: con = _pos + new Point(_width/2, _height); break;
				case 3: con = _pos + new Point(0,_height/2); break;
			}
			double curDist = target.distanceTo(con);
			if (curDist < dist) {
				dist = curDist;
				p = con;
			}
		}
		return p;
	}

}