part of grapheen;

class _PipeDedup extends _Pipe {
	_PipeDedup(Grapheen g,_Pipe prev):super(g,prev);

	bool get vertexType => _prev.vertexType;

	List ids() {
		List<int> inp = _prev.ids();
		List<int> ret = new List();
		inp.forEach((int id) {
			if (ret.contains(id))
				return;
			ret.add(id);
		});
		return ret;
	}
}

