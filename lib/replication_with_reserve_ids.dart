part of grapheen;

abstract class ReplicationWithReserveIds extends Replication {
	ReplicationWithReserveIds(Grapheen g):super(g) {
	}
	Future replicate([String url]) async {
		// collect negative Ids from queue
		List<int> negatives = new List();
		_queue.forEach((List event) {
			if (event[1] > 0)
				return;
			if (negatives.contains(event[1]))
				return;
			negatives.add(event[1]);
		});
		List<int> newids;
		if (negatives.length>0) {
			// call_reserveIds
			newids = await call_reserveIds(negatives.length);
		}
		// prepare input snapshot
		String input = "";
		_queue.forEach((List event) {
			List copyEvent = new List.from(event);
			if (copyEvent[1]<0) {
				copyEvent[1] = newids[negatives.indexOf(copyEvent[1])];
			}
			Object value = copyEvent[3];
			if (copyEvent[0] == false && (copyEvent[2] == "_from" || copyEvent[2] == "_to") && value<0) {
				copyEvent[3] = newids[negatives.indexOf(value)];
			}
			input += stringifyEvent(copyEvent);
			input += "\n";
		});
		// call_replicate
		String result = await call_replicate(input,url);

		// clear queue
		removeQueue();

		// fill in replicate data to engine
		_g.loadGraph(result);
	}
	Future<List<int>> call_reserveIds(int cnt);

}