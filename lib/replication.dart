part of grapheen;

abstract class Replication {
	Grapheen _g;
	Grapheen get g => _g;
	List<List> _queue = new List();
	void addToQueue(bool vertex,int id, String key, Object value) {
		_queue.add([vertex,id,key,value]);
	}
	List<List> get queue => _queue;
	int _nextObjectId = -1;
	int newObjectId() {
		return _nextObjectId--;
	}
	Replication(this._g) {
	}
	void clear() {
		_queue = new List();
		_nextObjectId = -1;
	}
	Future replicate([String url]) async {
		// prepare input snapshot
		String input = "";
		_queue.forEach((List event) {
			input += stringifyEvent(event);
			input += "\n";
		});
		// call_replicate
		String result = await call_replicate(input,url);

		// clear queue
		removeQueue();

		// fill in replicate data to engine
		_g.loadGraph(result);
	}
	void removeQueue() {
		_queue.forEach((List event) {
			int id = event[1];
			if (id<0) {
				if (event[0] == true)
					_g._v.set(id,event[2],null);
				else
					_g._e.set(id,event[2],null);
			}
		});
		_queue.clear();
	}
	String stringifyEvent(List event) {
		String input = "";
		input += (event[0] ? "V," : "E,")+event[1].toString()+","+event[2]+",";
		Object value = event[3];
		if (value is String) {
			value = value.replaceAll("'","\\'");
			input += "'"+value+"'";
		}
		else
			input += value.toString();
		return input;
	}
	Future<String> call_replicate(String input, [String url]);
}