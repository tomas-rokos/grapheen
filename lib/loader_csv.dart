library graphee_loader_csv;

class LoaderCSV {
	String _s;
	int _currPos = 0;
	LoaderCSV(this._s);

	List<String> splitLine() {
		bool inString = false;
		bool inToken = false;
		List<String> res = new List();
		int currTokenStart = _currPos;
		for(;_currPos<_s.length;++_currPos) {
			int c = _s.codeUnitAt(_currPos);
			if (c == 39) { // '
				if (inString) {
					if (_s.codeUnitAt(_currPos-1) == 92)
						continue;
					res.add(_s.substring(currTokenStart,_currPos+1));
					inString = false;
					continue;
				}
				currTokenStart = _currPos;
				inString = true;
			}
			if (inString)
				continue;
			if (c == 10 || c == 13) {
				if (inToken) {
					res.add(_s.substring(currTokenStart, _currPos));
				}
				++_currPos;
				return res;
			} else if (c == 32) {
				if (inToken) {
					res.add(_s.substring(currTokenStart, _currPos));
					inToken = false;
				}
			} else if  (c == 44) { // ,
				if (inToken) {
					res.add(_s.substring(currTokenStart, _currPos));
					inToken = false;
				}
			} else {
				if (inToken == false) {
					inToken = true;
					currTokenStart = _currPos;
				}
			}
		}
		return res.isEmpty ? null : res;
	}
}