0.6.0
-----
-

0.5.0
-----
- sorting method: first numbers then strings
- has operators: <, >, <=, >=
- has operators and order based on sorting method

0.4.0
-----
- graph visualisation into HTML5 SVG
- has operators: =, !=, IN, CONTAINS

0.3.0
-----
- synchronous version
- in memory index
- replication

0.2.0
-----
- graph text loader
- CSV loader
- benchmark testing
- DEDUP
- ORDER

0.1.0
-----
- basic functionality of Gremlin