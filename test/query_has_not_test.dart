library query_has_not_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";
import "bands.dart";

void main() {
	group("Query Has Not -", () {
		Grapheen g;
		setUp(() {
			g = new Grapheen();
			fillInGraph(g);
		});
		tearDown(() {
		});
		test("single result", () {
			Pipe v = g.V().has("label","Band").hasNot("name","Queen");
			expect(v.ids(),unorderedEquals([15]));
		});
		test("single result - reversed", () {
			Pipe v = g.V().hasNot("name","Queen").has("label","Band");
			expect(v.ids(),unorderedEquals([15]));
		});
	});
}