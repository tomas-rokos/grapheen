library grapheen_data_bands;

import "package:grapheen/grapheen.dart";

void fillInGraph(Grapheen g) {
	var rock = g.addVertex({"label":"Genre","name":"Rock"});
	var metal = g.addVertex({"label":"Genre","name":"Heavy metal"});

	var queen = g.addVertex({"label":"Band","name":"Queen"});
	g.addEdge(queen,rock,"PLAYS");

	var frme = g.addVertex({"label":"Person","name":"Freddie Mercury", "born_as":"Farrokh Bulsara"});
	var brma = g.addVertex({"label":"Person","name":"Brian May"});
	var rota = g.addVertex({"label":"Person","name":"Roger Taylor"});
	var jode = g.addVertex({"label":"Person","name":"John Deacon"});

	var fq1 = g.addVertex({"label":"Formation"});
	g.addEdge(queen,fq1,"FORMED"); //10
	g.addEdge(fq1,frme,"VOCAL");
	g.addEdge(fq1,brma,"GUITAR");
	g.addEdge(fq1,rota,"DRUMS");
	g.addEdge(fq1,jode,"BASS");

	var irons = g.addVertex({"label":"Band","name":"Iron Maiden"});
	g.addEdge(irons,metal,"PLAYS");

	var brdu = g.addVertex({"label":"Person","name":"Bruce Dickinson"});
	var stha = g.addVertex({"label":"Person","name":"Steve Harris"});
	var damu = g.addVertex({"label":"Person","name":"Dave Murray"});
	var adsm = g.addVertex({"label":"Person","name":"Adrian Smith"});//20
	var nimb = g.addVertex({"label":"Person","name":"Nicko McBrain"});
	var jage = g.addVertex({"label":"Person","name":"Janick Gers"});

	var fi1 = g.addVertex({"label":"Formation","from":"2000"});
	g.addEdge(irons,fi1,"FORMED");
	g.addEdge(fi1,brdu,"VOCAL");
	g.addEdge(fi1,damu,"GUITAR");
	g.addEdge(fi1,adsm,"GUITAR");
	g.addEdge(fi1,jage,"GUITAR");
	g.addEdge(fi1,nimb,"DRUMS");
	g.addEdge(fi1,stha,"BASS");//30

	var pada = g.addVertex({"label":"Person","name":"Paul Di Anno"});
	var dest = g.addVertex({"label":"Person","name":"Dennis Stratton"});
	var clbu = g.addVertex({"label":"Person","name":"Clive Burr"});
	var blba = g.addVertex({"label":"Person","name":"Blaze Bayley"});

	var fi2 = g.addVertex({"label":"Formation","from":"1979","to":"1980"});
	g.addEdge(irons,fi2,"FORMED");
	g.addEdge(fi2,pada,"VOCAL",{"problem":"drugs"});
	g.addEdge(fi2,damu,"GUITAR");
	g.addEdge(fi2,dest,"GUITAR");
	g.addEdge(fi2,clbu,"DRUMS");//40
	g.addEdge(fi2,stha,"BASS");

	var fi3 = g.addVertex({"label":"Formation","from":"1980","to":"1981"});
	g.addEdge(irons,fi3,"FORMED");
	g.addEdge(fi3,pada,"VOCAL");
	g.addEdge(fi3,damu,"GUITAR");
	g.addEdge(fi3,adsm,"GUITAR");
	g.addEdge(fi3,clbu,"DRUMS");
	g.addEdge(fi3,stha,"BASS");

	var fi4 = g.addVertex({"label":"Formation","from":"1994","to":"1999"});
	g.addEdge(irons,fi4,"FORMED");//50
	g.addEdge(fi4,blba,"VOCAL",{"problem":"voice"});
	g.addEdge(fi4,damu,"GUITAR");
	g.addEdge(fi4,jage,"GUITAR");
	g.addEdge(fi4,nimb,"DRUMS");
	g.addEdge(fi4,stha,"BASS");

	var arbC = g.addVertex({"label":"Conflicting","from":"1980"});
}

//String setupDb = """
//		(:Album {name:'Iron Maiden',released: '14 April 1980'})-[:BAND]->(IRONS),
//		(:Album {name:'Killers',released: '2 February 1981'})-[:BAND]->(IRONS),
//		(:Album {name:'The Number of the Beast',released: '22 March 1982'})-[:BAND]->(IRONS),
//		(:Album {name:'Piece of Mind',released: '16 May 1983'})-[:BAND]->(IRONS),
//		(:Album {name:'Powerslave',released: '3 September 1984'})-[:BAND]->(IRONS),
//		(:Album {name:'Somewhere in Time',released: '29 September 1986'})-[:BAND]->(IRONS),
//		(:Album {name:'Seventh Son of a Seventh Son',released: '11 April 1988'})-[:BAND]->(IRONS),
//		(:Album {name:'No Prayer for the Dying',released: '1 October 1990'})-[:BAND]->(IRONS),
//		(:Album {name:'Fear of the Dark',released: '11 May 1992'})-[:BAND]->(IRONS),
//		(:Album {name:'The X Factor',released: '2 October 1995'})-[:BAND]->(IRONS),
//		(:Album {name:'Virtual XI',released: '23 March 1998'})-[:BAND]->(IRONS),
//		(:Album {name:'Brave New World',released: '29 May 2000'})-[:BAND]->(IRONS),
//		(:Album {name:'Dance of Death',released: '8 September 2003'})-[:BAND]->(IRONS),
//		(:Album {name:'A Matter of Life and Death',released: '25 August 2006'})-[:BAND]->(IRONS),
//		(:Album {name:'The Final Frontier',released: '16 August 2010'})-[:BAND]->(IRONS),
//		(:Album:Live {name:'Live After Death',released:'14 October 1985'})-[:BAND]->(IRONS),
//		(:Album:Live {name:'A Real Live One',released:'22 March 1993'})-[:BAND]->(IRONS),
//		(:Album:Live {name:'A Real Dead One',released:'18 October 1993'})-[:BAND]->(IRONS),
//		(:Album:Live {name:'Live at Donington',released:'8 November 1993'})-[:BAND]->(IRONS),
//		(:Album:Live {name:'Rock in Rio',released:'25 March 2002'})-[:BAND]->(IRONS),
//		(:Album:Live {name:'BBC Archives',released:'4 November 2002'})-[:BAND]->(IRONS),
//		(:Album:Live {name:'Beast over Hammersmith',released:'4 November 2002'})-[:BAND]->(IRONS),
//		(:Album:Live {name:'Death on the Road',released:'29 August 2005'})-[:BAND]->(IRONS),
//		(:Album:Live {name:'Flight 666',released:'25 May 2009'})-[:BAND]->(IRONS),
//		(:Album:Live {name:'En Vivo!',released:'26 March 2012'})-[:BAND]->(IRONS),
//		(:Album:Live {name:'Maiden England 88',released:'25 March 2013'})-[:BAND]->(IRONS),
//		(:Album:Compilation {name:'Best of the Beast',released:'23 September 1996'})-[:BAND]->(IRONS),
//		(:Album:Compilation {name:'Ed Hunter',released:'July 1999'})-[:BAND]->(IRONS),
//		(:Album:Compilation {name:'Best of the B Sides',released:'4 November 2002'})-[:BAND]->(IRONS),
//		(:Album:Compilation {name:'Edward the Great',released:'4 November 2002'})-[:BAND]->(IRONS),
//		(:Album:Compilation {name:'The Essential Iron Maiden',released:'5 July 2005'})-[:BAND]->(IRONS),
//		(:Album:Compilation {name:'Somewhere Back in Time The Best of: 1980 – 1989',released:'12 May 2008'})-[:BAND]->(IRONS),
//		(:Album:Compilation {name:'From Fear to Eternity The Best of 1990 – 2010',released:'6 June 2011'})-[:BAND]->(IRONS),
//
//		(GNR:Band {name: 'Guns N Roses'})-[:PLAYS]->(METAL),
//
//		(AXRO:Person {name: 'Axl Rose'}),
//		(SLASH:Person {name: 'Slash'}),
//		(IZST:Person {name: 'Izzy Stradlin'}),
//		(DUMK:Person {name: 'Duff McKagan'}),
//		(STAD:Person {name: 'Steven Adler'}),
//
//		(FG1:Formation {from:'1985',to:'1990'})-[:BAND]->(GNR),
//		(FG1)-[:VOCAL]->(AXRO),
//		(FG1)-[:BASS]->(DUMK),
//		(FG1)-[:GUITAR]->(SLASH),
//		(FG1)-[:GUITAR]->(IZST),
//		(FG1)-[:DRUMS]->(STAD),
//
//		(DIRE:Person {name: 'Dizzy Reed'}),
//		(MASO:Person {name: 'Matt Sorum'}),
//
//		(FG2:Formation {from:'1990',to:'1991'})-[:BAND]->(GNR),
//		(FG2)-[:VOCAL]->(AXRO),
//		(FG2)-[:BASS]->(DUMK),
//		(FG2)-[:GUITAR]->(SLASH),
//		(FG2)-[:GUITAR]->(IZST),
//		(FG2)-[:KEYBOARD]->(DIRE),
//		(FG2)-[:DRUMS]->(MASO),
//
//		(GICK:Person {name: 'Gilby Clark'}),
//
//		(FG3:Formation {from:'1991',to:'1993'})-[:BAND]->(GNR),
//		(FG3)-[:VOCAL]->(AXRO),
//		(FG3)-[:BASS]->(DUMK),
//		(FG3)-[:GUITAR]->(SLASH),
//		(FG3)-[:GUITAR]->(GICK),
//		(FG3)-[:KEYBOARD]->(DIRE),
//		(FG3)-[:DRUMS]->(MASO),
//
//		(:Album {name:'Appetite for Destruction',released:'July 21, 1987'})-[:BAND]->(GNR),
//		(:Album {name:'G N R Lies',released:'November 29, 1988'})-[:BAND]->(GNR),
//		(:Album {name:'Use Your Illusion I',released:'September 17, 1991'})-[:BAND]->(GNR),
//		(:Album {name:'Use Your Illusion II',released:'September 17, 1991'})-[:BAND]->(GNR),
//		(:Album {name:'The Spaghetti Incident?',released:'November 23, 1993'})-[:BAND]->(GNR),
//		(:Album {name:'Chinese Democracy',released:'November 23, 2008'})-[:BAND]->(GNR),
//		(:Album:Live {name:'Live Era 87–93',released:'November 23, 1999'})-[:BAND]->(GNR),
//		(:Album:Live {name:'Appetite for Democracy',released:'November 4, 2014'})-[:BAND]->(GNR),
//		(:Album:Live {name:'Live Radio Broadcasts',released:'April 28, 2015'})-[:BAND]->(GNR),
//		(:Album:Compilation {name:'Use Your Illusion',released:'August 25, 1998'})-[:BAND]->(GNR),
//		(:Album:Compilation {name:'Greatest Hits',released:'March 23, 2004'})-[:BAND]->(GNR),
//""";