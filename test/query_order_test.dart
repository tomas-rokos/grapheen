library query_order_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";
import "bands.dart";

void main() {
	group("Query Order -", () {
		Grapheen g;
		int i = 0;
		setUp(() {
			g = new Grapheen();
			fillInGraph(g);
			i = g.addVertex({"label":"fororder","fororder":"A"}).ids().first;
			g.addVertex({"label":"fororder","fororder":34});
			g.addVertex({"label":"fororder","fororder":10});
			g.addVertex({"label":"fororder","fororder":"a"});
			g.addVertex({"label":"fororder","fororder":15.5});
			g.addVertex({"label":"fororder","fororder":"ZZ"});
			g.addVertex({"label":"fororder","fororder":-3.4});
		});
		tearDown(() {
		});
		test("simple string property", () {
			Pipe v = g.V(9).outE().inV().order("name");   // formation of Queen
			expect(v.ids(),orderedEquals([6,5,8,7])); // QUEEN members
		});
		test("simple string property for edges", () {
			Pipe v = g.V("label","Formation").outE("VOCAL").order("problem");
			expect(v.ids(),orderedEquals([37,51,11,25,44]));
		});
		test("mixed type property", () {
			Pipe v = g.V("label","fororder").order("fororder");
			expect(v.ids(),orderedEquals([i+6,i+2,i+4,i+1,i,i+5,i+3]));
		});
	});
}