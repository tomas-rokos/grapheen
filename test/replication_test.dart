library replication_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";
import "replication_fake.dart";

void main() {
	group("Replication -", () {
		ReplicationFake r;
		Grapheen g;
		setUp(() {
			g = new Grapheen();
			r = new ReplicationFake(g);
			g.replication = r;
		});
		tearDown(() {
		});
		test("initial", () async {
			await r.replicate();
			expect(g.dump(),"V,1,name,'Name 1'\nV,2,name,'Name 2'\nV,3,name,'Name 3'\n");
		});
		test("follow up", () async {
			await r.replicate();
			expect(g.dump(),"V,1,name,'Name 1'\nV,2,name,'Name 2'\nV,3,name,'Name 3'\n");
			await r.replicate();
			expect(g.dump(),"V,1,name,'Name 1'\nV,2,name,'Name 2'\nV,3,name,'Name 3'\nV,4,name,'Name 4'\n");
		});
		test("single new", () async {
			await r.replicate();
			expect(g.dump(),"V,1,name,'Name 1'\nV,2,name,'Name 2'\nV,3,name,'Name 3'\n");
			g.addVertex({"name":"local"});
			expect(g.dump(),"V,-1,name,'local'\nV,1,name,'Name 1'\nV,2,name,'Name 2'\nV,3,name,'Name 3'\n");
		});
		test("single new and replicate", () async {
			await r.replicate();
			g.addVertex({"name":"local"});
			await r.replicate();
			expect(g.dump(),"V,1,name,'Name 1'\nV,2,name,'Name 2'\nV,3,name,'Name 3'\nV,4,name,'local'\nV,5,name,'Name 5'\n");
		});
		test("new vertex and new edge and then replicate", () async {
			await r.replicate();
			Pipe p1 = g.addVertex({"name":"local"});
			g.addEdge(p1,g.V(3),"based");
			expect(g.dump(),"E,-2,_from,-1,_to,3,_name,'based'\nV,-1,name,'local'\nV,1,name,'Name 1'\nV,2,name,'Name 2'\nV,3,name,'Name 3'\n");
			await r.replicate();
			expect(g.dump(),"V,1,name,'Name 1'\nV,2,name,'Name 2'\nV,3,name,'Name 3'\nV,4,name,'local'\nE,5,_from,4,_to,3,_name,'based'\nV,6,name,'Name 6'\n");
		});
		test("new vertex and new edge and then two replicates", () async {
			await r.replicate();
			Pipe p1 = g.addVertex({"name":"local"});
			g.addEdge(p1,g.V(3),"based");
			await r.replicate();
			await r.replicate();
			expect(g.dump(),"V,1,name,'Name 1'\nV,2,name,'Name 2'\nV,3,name,'Name 3'\nV,4,name,'local'\nE,5,_from,4,_to,3,_name,'based'\nV,6,name,'Name 6'\nV,7,name,'Name 7'\n");
		});
	});
}