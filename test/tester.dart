@TestOn("vm")

import "package:test/test.dart";

import "create_vertexes_test.dart" as create_vertexes;
import "create_edges_test.dart" as create_edges;
import "query_vertex_test.dart" as query_vertex;
import "query_edge_test.dart" as query_edge;
import "query_has_test.dart" as query_has;
import "query_has_conditions_test.dart" as query_has_conditions;
import "query_has_not_test.dart" as query_has_not;
import "query_in_out_e_test.dart" as query_in_out_e;
import "query_in_out_v_test.dart" as query_in_out_v;
import "set_property_test.dart" as set_property;
import "remove_test.dart" as remove;
import "loader_csv_test.dart" as loader_csv;
import "query_dedup_test.dart" as query_dedup;
import "query_order_test.dart" as query_order;
import "replication_test.dart" as replication;
import "fixed_defects_test.dart" as fixed_defetcs;

void main() {
	// features
	create_vertexes.main();
	create_edges.main();
	query_vertex.main();
	query_edge.main();
	query_has.main();
	query_has_conditions.main();
	query_has_not.main();
	query_in_out_e.main();
	query_in_out_v.main();
	set_property.main();
	remove.main();
	loader_csv.main();
	query_dedup.main();
	query_order.main();
	replication.main();


	// fixed defects
	fixed_defetcs.main();
}


