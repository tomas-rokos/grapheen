library set_property_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";

void main() {
	group("Set Property -", () {
		Grapheen g;
		setUp(() {
			g = new Grapheen();
		});
		tearDown(() {
		});
		test("single property", () {
			Pipe v = g.addVertex();
			v.setProperty("name","vertex");
			expect(g.dump(),"V,1,name,'vertex'\n");
		});
		test("single property two vertexes", () {
			Pipe v1 = g.addVertex();
			Pipe v2 = g.addVertex();
			v1.setProperty("name","vertex");
			v2.setProperty("name","vertex2");
			v1.setProperty("name","vertex1");
			expect(g.dump(),"V,1,name,'vertex1'\nV,2,name,'vertex2'\n");
		});
		test("removal of property", () {
			Pipe v1 = g.addVertex();
			Pipe v2 = g.addVertex();
			v1.setProperty("name","vertex");
			v2.setProperty("name","vertex2");
			v1.setProperty("name",null);
			expect(g.dump(),"V,2,name,'vertex2'\n");
		});
	});
}