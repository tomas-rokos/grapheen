library query_edge_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";
import "bands.dart";

void main() {
	group("Query Edge -", () {
		Grapheen g;
		setUp(() {
			g = new Grapheen();
			fillInGraph(g);
		});
		tearDown(() {
		});
		test("by ID", () {
			Pipe v = g.E(16); // should be Irons plays Metal
			expect(v.ids(),unorderedEquals([16]));
			Map mp = v.map();
			expect(mp["_name"],"PLAYS");
			expect(mp["_from"],15);
			expect(mp["_to"],2);
		});
	});
}