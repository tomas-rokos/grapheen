library remove_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";

void main() {
	group("Remove -", () {
		Grapheen g;
		setUp(() {
			g = new Grapheen();
		});
		tearDown(() {
		});
		test("one in one out", () {
			Pipe v = g.addVertex({"test":"testik"});
			v.remove();
			expect(g.dump(),"");
		});
		test("two in first out", () {
			Pipe v1 = g.addVertex({"test":"testik"});
			Pipe v2 = g.addVertex({"test":"testik2"});
			v1.remove();
			expect(g.dump(),"V,2,test,'testik2'\n");
		});
		test("two in first out one in", () {
			Pipe v1 = g.addVertex({"test":"testik1"});
			g.addVertex({"test":"testik2"});
			v1.remove();
			g.addVertex({"test":"testik3"});
			expect(g.dump(),"V,2,test,'testik2'\nV,3,test,'testik3'\n");
		});
		test("two in second out one in", () {
			g.addVertex({"test":"testik"});
			Pipe v = g.addVertex({"test":"testik2"});
			v.remove();
			g.addVertex({"test":"testik3"});
			expect(g.dump(),"V,1,test,'testik'\nV,3,test,'testik3'\n");
		});
		test("two in, clear, one in", () {
			Pipe v1 = g.addVertex({"test":"testik1"});
			g.addVertex({"test":"testik2"});
			g.clear();
			g.addVertex({"test":"testik3"});
			expect(g.dump(),"V,1,test,'testik3'\n");
		});
	});
}