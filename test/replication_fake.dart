import "package:grapheen/grapheen.dart";
import "package:grapheen/loader_csv.dart";
import "dart:async";
import "dart:math";

class ReplicationFake extends Replication {
	int _objs = 0;
	ReplicationFake(Grapheen g):super(g) {
	}
	int rand() {
		return new Random(new DateTime.now().millisecondsSinceEpoch).nextInt(10)+2;
	}
	Future<String> call_replicate(String input, [String url]) {
		if (_objs == 0) {
			return new Future.value(_generateVertexes(3));
		}
		String s = "";
		if (input.length> 0) {
			LoaderCSV loader =  new LoaderCSV(input);
			Map<int,int> repliIds = new Map();
			while (true) {
				List<String> row = loader.splitLine();
				if (row == null)
					break;
				int rid = 0;
				try {
					rid = int.parse(row[1]);
				} catch (e) {
					print("ERROR("+row[1]+"):really strange ID:");
					continue;
				}
				if (rid<0 && repliIds.containsKey(rid) == false) {
					repliIds[rid] = ++_objs;
				}
				for (int i = 2;i<row.length;i+=2) {
					Object val;
					if (i+1 >=row.length) {
						print ("ERROR("+rid.toString()+"): no property pair");
						continue;
					}
					String vals = row[i+1];
					if (vals.codeUnitAt(0) == 39) {
						String subVal = vals.substring(1,vals.length-1);
						val = subVal.replaceAll("\\'","'");
					} else {
						try {
							val = int.parse(vals);
						} catch (e) {
							print("ERROR("+rid.toString()+"): cannot covert number: "+vals);
						}
					}
					rid = repliIds.containsKey(rid) == true ? repliIds[rid] : rid;
					if (row[0] == "V") {
						s += stringifyEvent([true,rid,row[i],val]);
					} else {
						if (row[i] == "_from" || row[i] == "_to")
							val = repliIds.containsKey(val) == true ? repliIds[val] : val;
						s += stringifyEvent([false,rid,row[i],val]);
					}
					s += "\n";
				}
			}

		}
		s += _generateVertexes(1);
		return new Future.value(s);
	}
	String _generateVertexes(num cnt) {
		String s = "";
		for (int i=0;i<cnt;++i) {
			s+="V,"+(++_objs).toString();
			s+=",name,'Name "+_objs.toString()+"'";
			s+="\n";
		}
		return s;
	}
}