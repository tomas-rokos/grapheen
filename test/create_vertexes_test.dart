library create_vertexes_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";

void main() {
	group("Create Vertexes -", () {
		Grapheen g;
		setUp(() {
			g = new Grapheen();
		});
		tearDown(() {
		});
		test("nothing", () {
			expect(g.dump(),"");
		});
		test("empty", () {
			g.addVertex();
			expect(g.dump(),"");
		});
		test("with props", () {
			g.addVertex({'one':'two','three':'four','five' : 5});
			expect(g.dump(),"V,1,one,'two',three,'four',five,5\n");
		});
		test("two with props", () {
			g.addVertex({'one':'two','three':'four','five' : 5});
			g.addVertex({'some':'another','new':'old','real' : 2.34});
			expect(g.dump(),"V,1,one,'two',three,'four',five,5\nV,2,some,'another',new,'old',real,2.34\n");
		});
		test("check result for id", () {
			g.addVertex({'one':'two','three':'four','five' : 5});
			Pipe v = g.addVertex({'some':'another','new':'old','real' : 2.34});
			expect(v.ids(),unorderedEquals([2]));
		});
		test("check result for props", () {
			g.addVertex({'one':'two','three':'four','five' : 5});
			Pipe v = g.addVertex({'some':'another','new':'old','real' : 2.34});
			Map mp = v.map();
			expect(mp.keys,unorderedEquals(["some","new","real"]));
			expect(mp["some"],"another");
			expect(mp["new"],"old");
			expect(mp["real"],2.34);

		});
	});
}