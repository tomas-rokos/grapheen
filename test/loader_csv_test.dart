library loader_csv_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";

void main() {
	group("LoaderCSV -", () {
		Grapheen g;
		setUp(() {
			g = new Grapheen();
		});
		tearDown(() {
		});
		test("empty", () {
			g.loadGraph("");
			expect(g.dump(),"");
		});
		test("string values", () {
			g.loadGraph("V,1,title,'Mr.',name,'Tomas',surname,'Rokos'\n");
			expect(g.dump(),"V,1,title,'Mr.',name,'Tomas',surname,'Rokos'\n");
		});
		test("int values", () {
			g.loadGraph("V,1,title,30,name,'Tomas',surname,-5\n");
			expect(g.dump(),"V,1,title,30,name,'Tomas',surname,-5\n");
		});
		test("more records - string values", () {
			g.loadGraph("V,1,title,'Mr.',name,'Tomas',surname,'Rokos'\nV,2,title,'Mr.',name,'Edgar',surname,'Poe'\n");
			expect(g.dump(),"V,1,title,'Mr.',name,'Tomas',surname,'Rokos'\nV,2,title,'Mr.',name,'Edgar',surname,'Poe'\n");
		});
		test("escaped apostrophe", () {
			String s = "V,1,name,'Tomas\\'s'\n";
			g.loadGraph(s);
			expect(g.dump(),s);
		});
	});
}