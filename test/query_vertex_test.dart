library query_vertex_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";
import "bands.dart";

void main() {
	group("Query Vertex -", () {
		Grapheen g;
		setUp(() {
			g = new Grapheen();
			fillInGraph(g);
		});
		tearDown(() {
		});
		test("by ID", () {
			Pipe v = g.V(17); // should be Bruce Dickinson
			expect(v.ids(),unorderedEquals([17]));
			Map mp = v.map();
			expect(mp["name"],"Bruce Dickinson");
			expect(mp["label"],"Person");
		});
		test("by property", () {
			Pipe v = g.V("name","Steve Harris");
			expect(v.ids(),unorderedEquals([18]));
			Map mp = v.map();
			expect(mp["name"],"Steve Harris");
			expect(mp["label"],"Person");
		});
	});
}