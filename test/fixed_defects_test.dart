library fixed_defects_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";
import "bands.dart";

void main() {
	group("Fixed defects -", () {
		Grapheen g;
		setUp(() {
			g = new Grapheen();
			fillInGraph(g);
		});
		tearDown(() {
		});
		test("sort via missing property", () {
			List<int> ids = g.V("label","Formation").order("to").ids();
			expect(ids,orderedEquals([35,42,49,9,23]));
		});
		test("query via missing property", () {
			List<int> ids = g.V("jeden","jeden").ids();
			expect(ids,orderedEquals([]));
		});
		test("edge is left behind after vertex delete", () {
			g.V(31).remove();
			g.V("label","Formation").outE("VOCAL").inV().order("name").ids();
		});
	});
}