library query_in_out_v_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";
import "bands.dart";

void main() {
	group("Query In/Out V -", () {
		Grapheen g;
		setUp(() {
			g = new Grapheen();
			fillInGraph(g);
		});
		tearDown(() {
		});
		test("inV", () {
			Pipe v = g.V("name","Queen").outE("PLAYS").inV();
			expect(v.ids(),unorderedEquals([1])); // ROCK
		});
		test("outV", () {
			Pipe v = g.V(9).inE().outV();   // formation of Queen
			expect(v.ids(),unorderedEquals([3])); // QUEEN
		});
		test("inV based on outE with name", () {
			Pipe v = g.V(9).outE("BASS").inV();   // formation of Queen
			Map obj = v.map();
			expect(obj["name"],"John Deacon");
		});
		test("inV based on outE - multiple results", () {
			Pipe v = g.V(9).outE().inV();   // formation of Queen
			expect(v.ids(),unorderedEquals([5,6,7,8])); // QUEEN members
		});
	});
}