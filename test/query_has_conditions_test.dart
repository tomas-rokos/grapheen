library query_has_conditions_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";
import "bands.dart";

void main() {
	group("Query Has With Conditions -", () {
		Grapheen g;
		int i;
		setUp(() {
			g = new Grapheen();
			fillInGraph(g);
			i = g.addVertex({"label":"fororder","fororder":"A"}).ids().first;
			g.addVertex({"label":"fororder","fororder":34});
			g.addVertex({"label":"fororder","fororder":10});
			g.addVertex({"label":"fororder","fororder":"a"});
			g.addVertex({"label":"fororder","fororder":15.5});
			g.addVertex({"label":"fororder","fororder":"ZZ"});
			g.addVertex({"label":"fororder","fororder":-3.4});
		});
		tearDown(() {
		});
		test("explicit IN", () {
			Pipe v = g.V().has("name","IN",["Steve Harris","Iron Maiden"]);
			expect(v.ids(),unorderedEquals([15,18]));
		});
		test("implicit IN", () {
			Pipe v = g.V().has("name",["Steve Harris","Iron Maiden"]);
			expect(v.ids(),unorderedEquals([15,18]));
		});
		test("CONTAINS", () {
			Pipe v = g.V().has("name","CONTAINS","Steve");
			expect(v.ids(),unorderedEquals([18]));
		});
		test("CONTAINS with Array", () {
			Pipe v = g.V().has("name","CONTAINS",["Steve","Burr"]);
			expect(v.ids(),unorderedEquals([18,33]));
		});
		test("less then for strings", () {
			Pipe v = g.V().has("name","<","Blaze Bayley");
			expect(v.ids(),unorderedEquals([20]));

		});
		test("less and equal then for strings", () {
			Pipe v = g.V().has("name","<=","Blaze Bayley");
			expect(v.ids(),unorderedEquals([20,34]));

		});
		test("greater then for strings", () {
			Pipe v = g.V().has("name",">","Roger Taylor");
			expect(v.ids(),unorderedEquals([18]));

		});
		test("greater then and equal to for strings", () {
			Pipe v = g.V().has("name",">=","Roger Taylor");
			expect(v.ids(),unorderedEquals([7,18]));

		});
		test("less then for mixed property", () {
			Pipe v = g.V().has("fororder","<","A");
			expect(v.ids(),unorderedEquals([i+1,i+2,i+4,i+6]));

		});
		test("greater then for mixed property", () {
			Pipe v = g.V().has("fororder",">","A");
			expect(v.ids(),unorderedEquals([i+3,i+5]));

		});
	});
}