library query_dedup_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";
import "bands.dart";

void main() {
	group("Query Dedup -", () {
		Grapheen g;
		setUp(() {
			g = new Grapheen();
			fillInGraph(g);
		});
		tearDown(() {
		});
		test("basic reduction", () {
			Pipe v = g.V("label","Formation").outE().inV();
			List<int> ids = v.ids();
			expect(ids.length,25);
			ids = v.dedup().ids();
			expect(ids.length,14);
		});
	});
}