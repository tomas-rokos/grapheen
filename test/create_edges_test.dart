library create_edges_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";

void main() {
	group("Create Edges -", () {
		Grapheen g;
		setUp(() {
			g = new Grapheen();
		});
		tearDown(() {
		});
		test("empty", ()  {
			Pipe v1 = g.addVertex();
			Pipe v2 = g.addVertex();
			Pipe e1 = g.addEdge(v1,v2,"relates");
			expect(g.dump(),"E,3,_from,1,_to,2,_name,'relates'\n");
		});
		test("with props", () {
			Pipe v1 = g.addVertex();
			Pipe v2 = g.addVertex();
			Pipe e1 = g.addEdge(v1,v2,"relates",{"test":"string","value":45});
			expect(g.dump(),"E,3,_from,1,_to,2,_name,'relates',test,'string',value,45\n");
		});
		test("check resulting Pipe object", () {
			Pipe v1 = g.addVertex();
			Pipe v2 = g.addVertex();
			Pipe e1 = g.addEdge(v1,v2,"relates",{"test":"string","value":45});
			expect(e1.ids(),unorderedEquals([3]));
			Map mp = e1.map();
			expect(mp.keys,unorderedEquals(["test","value","_from","_to","_name"]));
			expect(mp["test"],"string");
			expect(mp["_from"],1);
			expect(mp["_to"],2);
			expect(mp["_name"],"relates");
			expect(mp["value"],45);
		});
	});
}