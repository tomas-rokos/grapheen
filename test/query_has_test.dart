library query_has_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";
import "bands.dart";

void main() {
	group("Query Has -", () {
		Grapheen g;
		setUp(() {
			g = new Grapheen();
			fillInGraph(g);
		});
		tearDown(() {
		});
		test("single result", () {
			Pipe v = g.V().has("name","Steve Harris");
			expect(v.ids(),unorderedEquals([18]));
			Map mp = v.map();
			expect(mp["name"],"Steve Harris");
			expect(mp["label"],"Person");
		});
		test("multiple results", () {
			Pipe v = g.V().has("label","Band");
			expect(v.ids(),unorderedEquals([3,15]));
		});
		test("no results", () {
			Pipe v = g.V().has("label","Bandit");
			expect(v.ids(),unorderedEquals([]));
		});
		test("chained first part isolated", () {
			Pipe v = g.V().has("label","Formation");
			expect(v.ids(),unorderedEquals([9,23,35,42,49]));
		});
		test("chained second part isolated", () {
			Pipe v = g.V().has("from","1980");
			expect(v.ids(),unorderedEquals([42,56]));
		});
		test("chained", () {
			Pipe v = g.V().has("label","Formation").has("from","1980");
			expect(v.ids(),unorderedEquals([42]));
		});
		test("chained reversed", () {
			Pipe v = g.V().has("from","1980").has("label","Formation");
			expect(v.ids(),unorderedEquals([42]));
		});
		test("edge property", () {
			Pipe v = g.E().has("problem","voice");
			expect(v.ids(),unorderedEquals([51]));
		});
	});
}