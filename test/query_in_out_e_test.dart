library query_in_out_e_test;

@TestOn("vm")

import "package:test/test.dart";
import "package:grapheen/grapheen.dart";
import "bands.dart";

void main() {
	group("Query In/Out E -", () {
		Grapheen g;
		setUp(() {
			g = new Grapheen();
			fillInGraph(g);
		});
		tearDown(() {
		});
		test("in edges", () {
			Pipe v = g.V(9).inE(); // should be formation of Queen
			expect(v.ids(),unorderedEquals([10])); // edge to Queen
		});
		test("in edges with name - has result", () {
			Pipe v = g.V(9).inE("FORMED"); // should be formation of Queen
			expect(v.ids(),unorderedEquals([10])); // edge to Queen
		});
		test("in edges with name - no result", () {
			Pipe v = g.V(9).inE("FORM"); // should be formation of Queen
			expect(v.ids(),unorderedEquals([]));
		});
		test("out edges", () {
			Pipe v = g.V(9).outE(); // should be formation of Queen
			expect(v.ids(),unorderedEquals([11,12,13,14])); // edges to members
		});
		test("out edges with name - has result", () {
			Pipe v = g.V(9).outE("VOCAL"); // should be formation of Queen
			expect(v.ids(),unorderedEquals([11])); // edge to Freddie
		});
		test("out edges with name - no result", () {
			Pipe v = g.V(9).outE("FORM"); // should be formation of Queen
			expect(v.ids(),unorderedEquals([]));
		});
		test("both edges", () {
			Pipe v = g.V(9).bothE(); // should be formation of Queen
			expect(v.ids(),unorderedEquals([10,11,12,13,14])); // edges to members and band
		});
		test("both edges with name for in", () {
			Pipe v = g.V(9).bothE("FORMED"); // should be formation of Queen
			expect(v.ids(),unorderedEquals([10])); // edge to band
		});
		test("both edges with name for out", () {
			Pipe v = g.V(9).bothE("BASS"); // should be formation of Queen
			expect(v.ids(),unorderedEquals([14])); // edge to John
		});
		test("both edges with name - no result", () {
			Pipe v = g.V(9).bothE("BASSER"); // should be formation of Queen
			expect(v.ids(),unorderedEquals([])); // edge to John
		});
	});
}